<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220423041931 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE trailer (id INT AUTO_INCREMENT NOT NULL, trailer_type_id INT DEFAULT NULL, created_at DATETIME NOT NULL, last_update_at DATETIME DEFAULT NULL, webalize VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, is_active TINYINT(1) DEFAULT \'1\' NOT NULL, image_preview VARCHAR(255) DEFAULT NULL, sequence INT NOT NULL, website VARCHAR(255) NOT NULL, INDEX IDX_C691DC4E511F0C9D (trailer_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trailer_type (id INT AUTO_INCREMENT NOT NULL, is_for_hp TINYINT(1) DEFAULT \'0\' NOT NULL, created_at DATETIME NOT NULL, last_update_at DATETIME DEFAULT NULL, webalize VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, image_preview VARCHAR(255) DEFAULT NULL, sequence INT NOT NULL, is_active TINYINT(1) DEFAULT \'1\' NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE trailer ADD CONSTRAINT FK_C691DC4E511F0C9D FOREIGN KEY (trailer_type_id) REFERENCES trailer_type (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE trailer DROP FOREIGN KEY FK_C691DC4E511F0C9D');
        $this->addSql('DROP TABLE trailer');
        $this->addSql('DROP TABLE trailer_type');
    }
}
