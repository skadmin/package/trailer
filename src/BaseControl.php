<?php

declare(strict_types=1);

namespace Skadmin\Trailer;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'trailer';
    public const DIR_IMAGE = 'trailer';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-film']),
            'items'   => ['overview'],
        ]);
    }
}
