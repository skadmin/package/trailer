<?php

declare(strict_types=1);

namespace Skadmin\Trailer\Doctrine\Trailer;

use Doctrine\ORM\Mapping as ORM;
use Skadmin\Trailer\Doctrine\TrailerType\TrailerType;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Trailer
{
    use Entity\BaseEntity;
    use Entity\WebalizeName;
    use Entity\Description;
    use Entity\IsActive;
    use Entity\ImagePreview;
    use Entity\Sequence;
    use Entity\Website;

    #[ORM\Column]
    private string $preTitle = '';

    #[ORM\Column]
    private string $subTitle = '';


    #[ORM\ManyToOne(targetEntity: TrailerType::class, inversedBy: 'trailers')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private TrailerType $trailerType;

    public function update(string $name, string $preTitle, string $subTitle, string $description, bool $isActive, string $website, TrailerType $trailerType, ?string $imagePreview): void
    {
        $this->name        = $name;
        $this->preTitle    = $preTitle;
        $this->subTitle    = $subTitle;
        $this->description = $description;
        $this->website     = $website;

        $this->trailerType = $trailerType;

        $this->setIsActive($isActive);

        if ($imagePreview === null || $imagePreview === '') {
            return;
        }

        $this->imagePreview = $imagePreview;
    }

    public function getPreTitle(): string
    {
        return $this->preTitle;
    }

    public function getSubTitle(): string
    {
        return $this->subTitle;
    }

    public function getTrailerType(): TrailerType
    {
        return $this->trailerType;
    }
}
