<?php

declare(strict_types=1);

namespace Skadmin\Trailer\Doctrine\Trailer;

use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\Trailer\Doctrine\TrailerType\TrailerType;
use SkadminUtils\DoctrineTraits;
use SkadminUtils\DoctrineTraits\Facade;

final class TrailerFacade extends Facade
{
    use DoctrineTraits\Facade\Sequence;
    use DoctrineTraits\Facade\Webalize;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = Trailer::class;
    }

    public function create(string $name, string $preTitle, string $subTitle, string $description, bool $isActive, string $website, TrailerType $trailerType, ?string $imagePreview): Trailer
    {
        return $this->update(null, $name, $preTitle, $subTitle, false, $description, $isActive, $website, $trailerType, $imagePreview);
    }

    public function update(?int $id, string $name, string $preTitle, string $subTitle, bool $changeWebalize, string $description, bool $isActive, string $website, TrailerType $trailerType, ?string $imagePreview): Trailer
    {
        $trailer = $this->get($id);
        $trailer->update($name, $preTitle, $subTitle, $description, $isActive, $website, $trailerType, $imagePreview);

        if (! $trailer->isLoaded()) {
            $trailer->setSequence($this->getValidSequence());
            $trailer->setWebalize($this->getValidWebalize($name));
        } elseif ($changeWebalize) {
            $trailer->updateWebalize($this->getValidWebalize($name));
        }

        $this->em->persist($trailer);
        $this->em->flush();

        return $trailer;
    }

    public function get(?int $id = null): Trailer
    {
        if ($id === null) {
            return new Trailer();
        }

        $trailer = parent::get($id);

        if ($trailer === null) {
            return new Trailer();
        }

        return $trailer;
    }

    /**
     * @return Trailer[]
     */
    public function getAll(bool $onlyActive = false): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    public function findByWebalize(string $webalize): ?Trailer
    {
        $criteria = ['webalize' => $webalize];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

}
