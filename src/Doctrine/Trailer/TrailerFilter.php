<?php

declare(strict_types=1);

namespace Skadmin\Trailer\Doctrine\Trailer;

use Doctrine\Common\Collections\Criteria;
use Nette\SmartObject;
use Nette\Utils\DateTime;
use Skadmin\Trailer\Doctrine\TrailerType\TrailerType;
use SkadminUtils\DoctrineTraits\ACriteriaFilter;

use function count;

final class TrailerFilter extends ACriteriaFilter
{
    use SmartObject;

    /** @var array<TrailerType> */
    private array $types    = [];
    private ?bool $isActive = null;

    /**
     * @param array<TrailerType> $types
     */
    public function __construct(?bool $isActive = null, array $types = [])
    {
        $this->isActive = $isActive;
        $this->types    = $types;
    }

    public static function create(?bool $isActive = null, array $types = []): self
    {
        return new static($isActive, $types);
    }

    public function isActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return array<TrailerType>
     */
    public function getTypes(): array
    {
        return $this->types;
    }

    /**
     * @param array<TrailerType> $types
     */
    public function setTypes(array $types): void
    {
        $this->types = $types;
    }

    public function modifyCriteria(Criteria &$criteria, string $alias = 'a'): void
    {
        if (is_bool($this->isActive())) {
            $criteria->andWhere(Criteria::expr()->eq('a.isActive', $this->isActive()));
        }

        if (count($this->getTypes()) === 0) {
            return;
        }

        $criteria->andWhere(Criteria::expr()->in('t.id', $this->getTypes()));
    }
}
