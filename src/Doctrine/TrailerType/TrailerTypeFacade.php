<?php

declare(strict_types=1);

namespace Skadmin\Trailer\Doctrine\TrailerType;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\QueryBuilder;
use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

final class TrailerTypeFacade extends Facade
{
    use Facade\Sequence;
    use Facade\Webalize;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = TrailerType::class;
    }

    public function create(string $name, string $description, bool $isActive, bool $isForHp, ?string $imagePreview): TrailerType
    {
        return $this->update(null, $name, $description, $isActive, $isForHp, $imagePreview);
    }

    public function update(?int $id, string $name, string $description, bool $isActive, bool $isForHp, ?string $imagePreview): TrailerType
    {
        $trailerType = $this->get($id);
        $trailerType->update($name, $description, $isActive, $isForHp, $imagePreview);

        if (! $trailerType->isLoaded()) {
            $trailerType->setSequence($this->getValidSequence());
            $trailerType->setWebalize($this->getValidWebalize($name));
        }

        $this->em->persist($trailerType);
        $this->em->flush();

        return $trailerType;
    }

    public function get(?int $id = null): TrailerType
    {
        if ($id === null) {
            return new TrailerType();
        }

        $trailerType = parent::get($id);

        if ($trailerType === null) {
            return new TrailerType();
        }

        return $trailerType;
    }

    public function findByCode(string $code): ?TrailerType
    {
        $criteria = ['code' => $code];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    public function findByWebalize(string $webalize): ?TrailerType
    {
        $criteria = ['webalize' => $webalize];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    public function getCount(?TrailerTypeFilter $filter = null): int
    {
        $qb = $this->findQb($filter);
        $qb->select('count(distinct a.id)');

        return intval($qb->getQuery()
            ->getSingleScalarResult());
    }

    /**
     * @return TrailerType[]
     */
    public function find(?TrailerTypeFilter $filter = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->findQb($filter, $limit, $offset)
            ->getQuery()
            ->getResult();
    }

    private function findQb(?TrailerTypeFilter $filter = null, ?int $limit = null, ?int $offset = null): QueryBuilder
    {
        $qb = $this->em
            ->getRepository($this->table)
            ->createQueryBuilder('a');
        assert($qb instanceof QueryBuilder);

        $qb->setMaxResults($limit)
            ->setFirstResult($offset)
            ->orderBy('a.sequence', 'ASC');

        $criteria = Criteria::create();

        if ($filter !== null) {
            $filter->modifyCriteria($criteria);
        }

        $qb->addCriteria($criteria);

        return $qb;
    }
}
