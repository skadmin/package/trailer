<?php

declare(strict_types=1);

namespace Skadmin\Trailer\Doctrine\TrailerType;

use Doctrine\Common\Collections\Criteria;
use Nette\SmartObject;
use Nette\Utils\DateTime;
use SkadminUtils\DoctrineTraits\ACriteriaFilter;

use function count;
use function trim;

final class TrailerTypeFilter extends ACriteriaFilter
{
    use SmartObject;

    private ?bool $isActive = null;
    private ?bool $isForHp  = null;

    /** @var array<TrailerType> */
    private array $types = [];

    /**
     * @param array<TrailerType> $types
     */
    public function __construct(?bool $isActive, ?bool $isForHp, array $types)
    {
        $this->isActive = $isActive;
        $this->isForHp  = $isForHp;
        $this->types    = $types;
    }

    /**
     * @param array<TrailerType> $types
     */
    public static function create(?bool $isActive = null, ?bool $isForHp = null, array $types = []): self
    {
        return new self($isActive, $isForHp, $types);
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    public function getIsForHp(): ?bool
    {
        return $this->isForHp;
    }

    public function setIsForHp(?bool $isForHp): void
    {
        $this->isForHp = $isForHp;
    }

    /**
     * @return array<TrailerType>
     */
    public function getTypes(): mixed
    {
        return $this->types;
    }

    /**
     * @param array<TrailerType> $types
     */
    public function setTypes(mixed $types): void
    {
        $this->types = $types;
    }

    public function modifyCriteria(Criteria &$criteria, string $alias = 'a'): void
    {
        $expr = Criteria::expr();

        if (is_bool($this->getIsActive())) {
            $criteria->andWhere(Criteria::expr()->eq('a.isActive', $this->getIsActive()));
        }

        if (is_bool($this->getIsForHp())) {
            $criteria->andWhere(Criteria::expr()->eq('a.isForHp', $this->getIsActive()));
        }

        if (count($this->getTypes()) === 0) {
            return;
        }

        $criteria->andWhere(Criteria::expr()->in('a.id', $this->getTypes()));
    }
}
