<?php

declare(strict_types=1);

namespace Skadmin\Trailer\Doctrine\TrailerType;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Skadmin\Trailer\Doctrine\Trailer\Trailer;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class TrailerType
{
    use Entity\BaseEntity;
    use Entity\WebalizeName;
    use Entity\Description;
    use Entity\ImagePreview;
    use Entity\Sequence;
    use Entity\IsActive;
    use Entity\Code;

    #[ORM\Column(options: ['default' => false])]
    private bool $isForHp = false;

    /** @var ArrayCollection|Collection|Trailer[] */
    #[ORM\OneToMany(targetEntity: Trailer::class, mappedBy: 'trailerType')]
    private ArrayCollection|Collection|array $trailers;

    public function __construct()
    {
        $this->trailers = new ArrayCollection();
    }

    public function update(string $name, string $description, bool $isActive, bool $isForHp, ?string $imagePreview): void
    {
        $this->name        = $name;
        $this->description = $description;
        $this->isActive    = $isActive;
        $this->isForHp     = $isForHp;

        if ($imagePreview === null || $imagePreview === '') {
            return;
        }

        $this->imagePreview = $imagePreview;
    }

    public function isForHp(): bool
    {
        return $this->isForHp;
    }

    /**
     * @return ArrayCollection|Collection|Trailer[]
     */
    public function getTrailers(bool $onlyActive = false): ArrayCollection|Collection|array
    {
        $criteria = Criteria::create();

        if ($onlyActive) {
            $criteria->andWhere(Criteria::expr()->eq('isActive', true));
        }

        return $this->trailers->matching($criteria);
    }
}
