<?php

declare(strict_types=1);

namespace Skadmin\Trailer\Components\Admin;

interface IOverviewTypeFactory
{
    public function create(): OverviewType;
}
