<?php

declare(strict_types=1);

namespace Skadmin\Trailer\Components\Admin;

use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Trailer\BaseControl;
use Skadmin\Trailer\Doctrine\Trailer\Trailer;
use Skadmin\Trailer\Doctrine\Trailer\TrailerFacade;
use Skadmin\Trailer\Doctrine\TrailerType\TrailerTypeFacade;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class Overview extends GridControl
{
    use APackageControl;
    use IsActive;

    private TrailerFacade     $facade;
    private TrailerTypeFacade $facadeTrailerType;
    private LoaderFactory     $webLoader;
    private ImageStorage      $imageStorage;

    public function __construct(TrailerFacade $facade, TrailerTypeFacade $facadeTrailerType, Translator $translator, User $user, LoaderFactory $webLoader, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);

        $this->facade            = $facade;
        $this->facadeTrailerType = $facadeTrailerType;

        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'trailer.overview.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('jQueryUi')];
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DATA
        $dataTrailersType = $this->facadeTrailerType->getPairs('id', 'name');

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel()->orderBy('a.sequence', 'ASC'));

        // COLUMNS
        $grid->addColumnText('imagePreview', '')
            ->setRenderer(function (Trailer $trailer): ?Html {
                if ($trailer->getImagePreview() !== null) {
                    $imageSrc = $this->imageStorage->fromIdentifier([$trailer->getImagePreview(), '40x60', 'exact']);

                    return Html::el('img', [
                        'src'   => sprintf('/%s', $imageSrc->createLink()),
                        'style' => 'max-width: none;',
                    ]);
                }

                return null;
            })->setAlign('center');
        $grid->addColumnText('name', 'grid.trailer.overview.name')
            ->setRenderer(function (Trailer $trailer): Html {
                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit',
                        'id'      => $trailer->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $preSubTitle = Html::el('code', ['class' => 'text-muted small'])
                    ->setText(trim(implode(' • ', [$trailer->getPreTitle(), $trailer->getSubTitle()]), ' •'));

                $name->setText($trailer->getName())
                    ->addHtml('<br>')
                    ->addHtml($preSubTitle);

                return $name;
            });
        $grid->addColumnText('trailerType', 'grid.trailer.overview.trailer-type')
            ->setRenderer(fn(Trailer $t): string => strip_tags($t->getTrailerType()->getName()));
        $grid->addColumnText('website', 'grid.trailer.overview.website')
            ->setRenderer(static function (Trailer $trailer): ?Html {
                if ($trailer->getWebsite() === '') {
                    return null;
                }

                $icon = Html::el('small', ['class' => 'fas fa-external-link-alt mr-1']);

                return Html::el('a', [
                    'href'   => $trailer->getWebsite(),
                    'target' => '_blank',
                ])->setHtml($icon)
                    ->addText($trailer->getWebsite());
            });
        $this->addColumnIsActive($grid, 'trailer.overview');

        // STYLE
        $grid->getColumn('imagePreview')
            ->getElementPrototype('th')
            ->setAttribute('style', 'width: 1px');

        // FILTER
        $grid->addFilterText('name', 'grid.trailer.overview.name', ['name', 'preTitle', 'subTitle']);
        $grid->addFilterSelect('trailerType', 'grid.trailer.overview.trailer-type', $dataTrailersType, 'trailerType')
            ->setPrompt(Constant::PROMTP);
        $this->addFilterIsActive($grid, 'trailer.overview');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.trailer.overview.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        $grid->addToolbarButton('Component:default#1', 'grid.trailer.overview.action.trailer-type', [
            'package' => new BaseControl(),
            'render'  => 'overview-type',
        ])->setIcon('list')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.trailer.overview.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        $grid->addToolbarSeo(BaseControl::RESOURCE, $this->getTitle());

        // OTHER
        $grid->setDefaultSort(['sequence' => 'ASC']);
        $grid->setDefaultFilter(['isActive' => 1]);

        // SORTING
        $grid->setSortable();
        $grid->setSortableHandler($this->link('sort!'));

        return $grid;
    }

    public function handleSort(?string $itemId, ?string $prevId, ?string $nextId): void
    {
        $this->facade->sort($itemId, $prevId, $nextId);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $presenter->flashMessage('grid.trailer.overview.action.flash.sort.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
    }
}
