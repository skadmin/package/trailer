<?php

declare(strict_types=1);

namespace Skadmin\Trailer\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
