<?php

declare(strict_types=1);

namespace Skadmin\Trailer\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Trailer\BaseControl;
use Skadmin\Trailer\Doctrine\Trailer\Trailer;
use Skadmin\Trailer\Doctrine\Trailer\TrailerFacade;
use Skadmin\Trailer\Doctrine\TrailerType\TrailerTypeFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class Edit extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory     $webLoader;
    private TrailerFacade     $facade;
    private TrailerTypeFacade $facadeTrailerType;
    private Trailer           $trailer;
    private ImageStorage      $imageStorage;

    public function __construct(?int $id, TrailerFacade $facade, TrailerTypeFacade $facadeTrailerType, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->facade            = $facade;
        $this->facadeTrailerType = $facadeTrailerType;

        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->trailer = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->trailer->isLoaded()) {
            return new SimpleTranslation('trailer.edit.title - %s', $this->trailer->getName());
        }

        return 'trailer.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function render(): void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->trailer = $this->trailer;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // DATA
        $dataTrailersType = Arrays::map(
            $this->facadeTrailerType->getPairs('id', 'name'),
            fn(string $name): string => strip_tags($name)
        );

        // INPUT
        $form->addText('name', 'form.trailer.edit.name')
            ->setRequired('form.trailer.edit.name.req');
        $form->addText('preTitle', 'form.trailer.pre-title.name');
        $form->addText('subTitle', 'form.trailer.sub-title.name');
        $form->addText('website', 'form.trailer.edit.website');
        $form->addCheckbox('isActive', 'form.trailer.edit.is-active')
            ->setDefaultValue(true);

        $form->addImageWithRFM('imagePreview', 'form.trailer.edit.image-preview');

        $form->addSelect('trailerTypeId', 'form.trailer.edit.trailer-type-id', $dataTrailersType)
            ->setRequired('form.trailer.edit.trailer-type-id.req')
            ->setPrompt(Constant::PROMTP)
            ->setTranslator(null);

        if ($this->trailer->isLoaded()) {
            $form->addCheckbox('changeWebalize', Html::el('sup', [
                'title'          => $this->translator->translate('form.trailer.edit.change-webalize'),
                'class'          => 'far fa-fw fa-question-circle',
                'data-toggle'    => 'tooltip',
                'data-placement' => 'left',
            ]))->setTranslator(null);
        }

        // TEXT
        $form->addTextArea('description', 'form.trailer.edit.description');

        // BUTTON
        $form->addSubmit('send', 'form.trailer.edit.send');
        $form->addSubmit('sendBack', 'form.trailer.edit.send-back');
        $form->addSubmit('back', 'form.trailer.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->trailer->isLoaded()) {
            return [];
        }

        return [
            'preTitle'      => $this->trailer->getPreTitle(),
            'subTitle'      => $this->trailer->getSubTitle(),
            'name'          => $this->trailer->getName(),
            'description'   => $this->trailer->getDescription(),
            'isActive'      => $this->trailer->isActive(),
            'website'       => $this->trailer->getWebsite(),
            'trailerTypeId' => $this->trailer->getTrailerType()->getId(),
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->imagePreview, BaseControl::DIR_IMAGE);

        if ($this->trailer->isLoaded()) {
            if ($identifier !== null && $this->trailer->getImagePreview() !== null) {
                $this->imageStorage->delete($this->trailer->getImagePreview());
            }

            $trailer = $this->facade->update(
                $this->trailer->getId(),
                $values->name,
                $values->preTitle,
                $values->subTitle,
                $values->changeWebalize,
                $values->description,
                $values->isActive,
                $values->website,
                $this->facadeTrailerType->get($values->trailerTypeId),
                $identifier
            );
            $this->onFlashmessage('form.trailer.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $trailer = $this->facade->create(
                $values->name,
                $values->preTitle,
                $values->subTitle,
                $values->description,
                $values->isActive,
                $values->website,
                $this->facadeTrailerType->get($values->trailerTypeId),
                $identifier
            );
            $this->onFlashmessage('form.trailer.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $trailer->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }

}
