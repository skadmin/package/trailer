<?php

declare(strict_types=1);

namespace Skadmin\Trailer\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Trailer\BaseControl;
use Skadmin\Trailer\Doctrine\TrailerType\TrailerType;
use Skadmin\Trailer\Doctrine\TrailerType\TrailerTypeFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class EditType extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory     $webLoader;
    private TrailerTypeFacade $facade;
    private TrailerType       $trailerType;

    public function __construct(?int $id, TrailerTypeFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facade    = $facade;
        $this->webLoader = $webLoader;

        $this->trailerType = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->trailerType->isLoaded()) {
            return new SimpleTranslation('trailer-type.edit.title - %s', $this->trailerType->getName());
        }

        return 'trailer-type.edit.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('adminTinyMce')];
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editType.latte');

        $template->trailerType = $this->trailerType;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.trailer-type.edit.name')
            ->setRequired('form.trailer-type.edit.name.req');
        $form->addTextArea('description', 'form.trailer-type.edit.description');

        $form->addCheckbox('isActive', 'form.trailer-type.edit.Is-active')
            ->setDefaultValue(true);
        $form->addCheckbox('isForHp', 'form.trailer-type.edit.is-for-hp');

        $form->addImageWithRFM('imagePreview', 'form.trailer-type.edit.image-preview');

        // BUTTON
        $form->addSubmit('send', 'form.trailer-type.edit.send');
        $form->addSubmit('sendBack', 'form.trailer-type.edit.send-back');
        $form->addSubmit('back', 'form.trailer-type.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->trailerType->isLoaded()) {
            return [];
        }

        return [
            'name'        => $this->trailerType->getName(),
            'description' => $this->trailerType->getDescription(),
            'isActive'    => $this->trailerType->isActive(),
            'isForHp'     => $this->trailerType->isForHp(),
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->imagePreview, BaseControl::DIR_IMAGE);

        if ($this->trailerType->isLoaded()) {
            $trailerType = $this->facade->update(
                $this->trailerType->getId(),
                $values->name,
                $values->description,
                $values->isActive,
                $values->isForHp,
                $identifier
            );
            $this->onFlashmessage('form.trailer-type.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $trailerType = $this->facade->create(
                $values->name,
                $values->description,
                $values->isActive,
                $values->isForHp,
                $identifier
            );
            $this->onFlashmessage('form.trailer-type.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-type',
            'id'      => $trailerType->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-type',
        ]);
    }
}
